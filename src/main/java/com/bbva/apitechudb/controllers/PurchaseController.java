package com.bbva.apitechudb.controllers;

import com.bbva.apitechudb.models.PurchaseModel;
import com.bbva.apitechudb.services.PurchaseService;
import com.bbva.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id de la compra a añadir es " + purchase.getId());
        System.out.println("La id del usuario de la compra a añadir es " + purchase.getUserId());
        System.out.println("Los elementos de la compra son " + purchase.getPurchaseItems());

        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(result, result.getResponseHttpStatusCode());
    }

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("getPurchases");

        return new ResponseEntity<>(
                this.purchaseService.getPurchases(),
                HttpStatus.OK
        );
    }
}