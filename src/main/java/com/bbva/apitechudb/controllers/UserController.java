package com.bbva.apitechudb.controllers;

import com.bbva.apitechudb.models.UserModel;
import com.bbva.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers() {
        System.out.println("getPurchases");

        return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUsersById(@PathVariable String id) {
        System.out.println("getUsersById");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "User no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUsers");
        System.out.println("La id del user a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(this.userService.addUser(user), HttpStatus.CREATED);

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable  String id) {
        System.out.println("updateUser");
        System.out.println("La id del Usuario a actualizar en parámetro URL es " + id);
        System.out.println("La id del usuario a actualizar es " + user.getId());
        System.out.println("El nombre del usuario a actualizar es " + user.getName());
        System.out.println("La edad del usuario a actualizar es " + user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if(userToUpdate.isPresent()) {
            System.out.println("Usuario para actualizar encontrado, actualizando");
            this.userService.update(user);
        }

        return new ResponseEntity<>(user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String>deleteUser(@PathVariable String id) {
        System.out.println("deleteuser");
        System.out.println("La id del usuario a borrar " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Purchase no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}

