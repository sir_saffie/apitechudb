package com.bbva.apitechudb.services;

import com.bbva.apitechudb.models.ProductModel;
import com.bbva.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll en ProductService");

        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("findById en ProductService");

      return this.productRepository.findById(id);
    }

    public ProductModel addProduct(ProductModel product) {
        System.out.println("addProduct en Product  Service");

        return this.productRepository.save(product);
    }

    public ProductModel update(ProductModel product) {
        System.out.println("update en ProductService");

        return this.productRepository.save(product);
    }

    public boolean delete(String id) {
        System.out.println("delete el ProductService");
        boolean result = false;

        if(this.findById(id).isPresent() == true) {
            System.out.println("Producto encontrado, borrando");
            this.productRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}
