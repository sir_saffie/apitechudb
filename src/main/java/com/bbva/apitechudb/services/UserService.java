package com.bbva.apitechudb.services;

import com.bbva.apitechudb.models.UserModel;
import com.bbva.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("findAll en UserService");

        return this.userRepository.findAll();
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel addUser(UserModel user) {
        System.out.println("addUser en User Service");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println("update en UserService");

        return this.userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println("delete el UserService");
        boolean result = false;

        if(this.findById(id).isPresent() == true) {
            System.out.println("User encontrado, borrando");
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}
