package com.bbva.apitechudb.repositories;

import com.bbva.apitechudb.models.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {
}
